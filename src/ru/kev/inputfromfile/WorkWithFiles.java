package ru.kev.inputfromfile;

import java.io.*;

/**
 * Класс считывает выражение из файла input.txt, выполняет арифметическую операцию из выражения и записывает
 * ответ в файл output.txt
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class WorkWithFiles {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("D:\\Учёба\\ОП\\InputFromFile\\input.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("D:\\Учёба\\ОП\\InputFromFile\\output.txt"));
        String string;
        int result = 0;
        while ((string = reader.readLine()) != null) {
            result = split(string);
            System.out.println(string + " = " +  result);
            writer.write( result + "\n");
            writer.flush();
        }
    }

    /**
     * Метод делит выражение на три части, затем обращается к методу calc для выполнения арифметической операции
     * @param string выражение
     * @return значение выражения
     */
    private static int split(String string) {
        String[] splitArray = new String[3];
        int i = 0;
        for (String splitPart : string.split(" ")) {
            splitArray[i] = splitPart;
            i++;
        }
        int result = calc(splitArray);
        return result;
    }

    /**
     * Метод преобразовывает строковые части выражения в целые значения и выполняет арифметическое действие с этими значениями
     * @param splitArray выражение, разделённое на части
     * @return результат выражения
     */
    private static int calc(String[] splitArray) {
        int result = 0;
        int number1 = Integer.parseInt(splitArray[0]);
        int number2 = Integer.parseInt(splitArray[2]);
        switch (splitArray[1]) {
            case "+":
                result = number1 + number2;
                break;
            case "-":
                result = number1 - number2;
                break;
            case "/":
                result = number1 / number2;
                break;
            case "*":
                result = number1 * number2;
                break;
        }
        return result;
    }
}
